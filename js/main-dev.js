var redirect_url;

$(document).ready(function() {
	if( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
			$(".mute").hide();

	}
	function flashInit() {
		var hasFlash =	swfobject.hasFlashPlayerVersion("1");
		var isDesktop =	$(window).width() > 1024;
		var isMobile =	$(window).width() < 480;

		if (hasFlash && isDesktop) {
		    $('.wrapper').hide();
		    $('.flash-container').show();

		    var params = {};
		    params.wmode = "transparent";
			swfobject.embedSWF("splash.swf", "flash", "100%", "100%", "9.0.0",false,false,params);

		} else {

			$('.wrapper').show();
			$('.flash-container').hide();
			$("img.first-child").delay(100).fadeIn(1200);

			$(".mute").click(function() {
		    	$(".mute").toggleClass("off");
		    });

		    /* Begin Animation */

			$(".trigger, .navigation ul li").delay(200).on("click tap", function (){
				redirect_url = $(this).attr("data-url");

				playAudio();
			});
			$("#audio").attr('src', 'audio/rev.mp3').on("playing",function(){
				loop();
			});

		    var img = $('.animation span img'),
		        n = img.length,
		        c = 0;
		    img.not(':eq('+c+')').hide();


			function playAudio() {
				document.getElementById("audio").play();
		       	if ($(".mute").hasClass("off")) {
					document.getElementById('audio').muted = true;
				}
			}

			function loop() {
				if (c==n) {
					$(".fade-out").fadeIn(2200, function(){
					 	redirect();
					 });

				} else {
					img.eq(++c%n).fadeTo(80, 1, loop);
				}
			}

			function redirect() {
				setTimeout(function(){
					if (redirect_url == '') redirect_url = 'http://www.autoshowny.com/';
					var getApp = 'https://play.google.com/store/apps/details?id=com.avai.amp.nyauto&hl=en_GB';
					if (redirect_url == getApp && /iPhone|iPad|iPod/i.test(navigator.userAgent)) {
						redirect_url = 'https://itunes.apple.com/us/app/new-york-international-auto/id350263720?mt=8';
					}
					var defaultLink = 'http://www.autoshowny.com/';
					if (redirect_url == defaultLink && isMobile ) {
						redirect_url = 'http://1230.m.amp.avai.com/';
					}
					document.location = redirect_url;
				}, 10);
				$("body").remove();
		    }

		}
	}

	$(window).resize(function() {
		flashInit();
	});

	flashInit();




});